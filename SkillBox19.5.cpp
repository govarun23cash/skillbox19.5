﻿// SkillBox19.5.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Animal
{
public:
    virtual void doSomething() = 0;
};


class Dog : public Animal
{
public:
    virtual void doSomething() 
    {
        std::cout << "Гав-Гав\n" << std::endl;
    }
};

class Cat : public Animal
{
public:
    virtual void doSomething()
    {
        std::cout << "Мяу-Мяу\n" << std::endl;
    }
};

class Mouse : public Animal
{
public:
    virtual void doSomething()
    {
        std::cout << "Пи-Пи\n" << std::endl;
    }
};
int main()
{
    setlocale(LC_ALL, "Russian");

    Animal* a = new Dog();
    a->doSomething();
    delete a;

    Animal* b = new Cat();
    b->doSomething();
    delete b;

    Animal* c = new Mouse();
    c->doSomething();
    delete c;
}


// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
